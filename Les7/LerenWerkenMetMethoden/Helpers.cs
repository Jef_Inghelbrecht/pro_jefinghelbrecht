﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlackMagic
{
    class Helpers
    {
        // de tekst die we tonen gaan we parametriseren: we gaan
        // de tekst doorgeven via een parameter
        // zodanig dat de methoden (functies) abstract worden
        public static int LeesGeheelGetalIn(string titel)
        {
            Console.Write(titel);
            int getal = Convert.ToInt32(Console.ReadLine());
            return getal;
        }

        public static char LeesCharIn(string titel)
        {
            Console.Write(titel);
            char teken = Convert.ToChar(Console.ReadLine());
            return teken;
        }
    }
}
