﻿using System;

namespace OplossingEvaluatie2G2
{
    class Program
    {
        static void Main(string[] args)
        {
            // doe zolang dat je geen nee zegt
            bool leeg = false; // sentinel / wachter
            do
            {
                Console.WriteLine("Haal een snoepje uit de zak.");
                Console.Write("Is de snoepzak leeg? j/n: ");
                char antwoord = Console.ReadKey().KeyChar;
                if (antwoord == 'j')
                {
                    leeg = true;
                }

            } while (!leeg);

            while (!leeg)
            {
                Console.WriteLine("Haal een snoepje uit de zak.");
                Console.Write("Is de snoepzak leeg? j/n: ");
                char antwoord = Console.ReadKey().KeyChar;
                if (antwoord == 'j')
                {
                    leeg = true;
                }
            }
            Console.ReadLine();
        }

        static void ToontGetallenVan1TotEnMet100()
        {
            Console.WriteLine("Tabel van 1 tot en met 100");
            for (int i = 0; i < 10; i++)
            {
                int getal = 1;
                // lus om 1 rij van 10 getallen te schrijven
                for (int j = 1; j <= 10; j++)
                {
                    // Console.Write($"\t{(i * 10) + j,3}");
                    Console.Write($"\t{getal++,3}");
                }
                Console.WriteLine();
            }
        }
    }
}
