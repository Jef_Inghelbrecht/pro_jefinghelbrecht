﻿using System;

namespace ForDoordenker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Hoeveel sterren? ");
            int aantal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("For doordenker");
            for (int i = 1; i <= aantal; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            for (int i = aantal-1; i > 0; i--)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
