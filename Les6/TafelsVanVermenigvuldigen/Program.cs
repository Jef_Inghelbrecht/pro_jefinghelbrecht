﻿using System;

namespace TafelsVanVermenigvuldigen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tafels van vermenigvuldigen");
            for (byte i=1; i <= 10;  i++ )
            {
                Console.WriteLine($"De tafel van {i}");
                for (byte j = 1; j <= 10; j++)
                {
                    Console.WriteLine($"{i} * {j:00} = {(i * j):000}");
                }
                Console.WriteLine("-".PadRight(20, '-'));
            }
            Console.ReadKey();

            Console.WriteLine("Tafels van vermenigvuldigen rooster");
            for (byte i = 1; i <= 10; i++)
            {
                for (byte j = 1; j <= 10; j++)
                {
                    if (i != 1 && j == 1)
                    {
                        Console.Write(i);
                    }
                    else
                    {
                        Console.Write(' ');
                    }

                    Console.Write($"\t{i * j}");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

    }
}
