﻿using System;

namespace Lussen
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Lussen");
            // Een miljoen keer 10 toevoegen aan een getal
            int getal = 0;
            DateTime start = DateTime.Now;
            for (int i=1; i <= 1000000; i++)
            {
                getal += 10;
            }
            DateTime end = DateTime.Now;
            Console.WriteLine($"Tijd: {end - start}");
            Console.WriteLine($"Het is getal is {getal}.");
            Console.ReadLine();
        }
    }
}
