﻿using System;

namespace H4_schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Schoenverkoper");
            Console.Write("Hoeveel paar schoenen wens jij te kopen? ");
            int aantalPaarSchoenen = Convert.ToInt32(Console.ReadLine());
            int eenheidsPrijs;
            if (aantalPaarSchoenen >= 100)
            {
                eenheidsPrijs = 5;
            }
            else if (aantalPaarSchoenen >= 10)
            {
                eenheidsPrijs = 10;
            }
            else if (aantalPaarSchoenen == 1)
            {
                eenheidsPrijs = 5000;
            }
            else
            {
                eenheidsPrijs = 20;
            }
            Console.WriteLine($"Totale schoenenprijs is Euro {aantalPaarSchoenen * eenheidsPrijs}");
            Console.ReadKey();
        }
    }
}
