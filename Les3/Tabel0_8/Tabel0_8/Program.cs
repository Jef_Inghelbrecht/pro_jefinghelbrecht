﻿using System;

namespace Tabel0_8
{
    class Program
    {
        enum Weerstandkleuren { Black = ConsoleColor.Black, Brown, Red, Orange }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            sbyte getal1 = 0;
            // Eerste onderdeel opdracht
            Console.WriteLine($"\t{getal1++}\t{getal1++}\t{getal1++}\n\t{getal1++}\t{getal1++}\t{getal1++}\n\t{getal1++}\t{getal1++}\t{getal1++}");
            getal1 = 0;
            Console.WriteLine($"\t{getal1++:0.00}\t{getal1++:0.00}\t{getal1++:0.00}\n\t{getal1++:0.00}\t{getal1++:0.00}\t{getal1++:0.00}\n\t{getal1++:0.00}\t{getal1++:0.00}\t{getal1++:0.00}");
            float getal2 = 0.25f;
            Console.WriteLine($"\t{++getal2:0.00}\t{++getal2:0.00}\t{++getal2:0.00}\n\t{++getal2:0.00}\t{++getal2:0.00}\t{++getal2:0.00}\n\t{++getal2:0.00}\t{++getal2:0.00}\t{++getal2:0.00}");
            getal2 = 0.255f;
            Console.WriteLine($"\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}\n\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}\n\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}\t{getal2+=13.10f:0.00}");
            Console.BackgroundColor = (ConsoleColor) ((Weerstandkleuren)1);
          
        Console.ReadKey();
        }
    }
}
