﻿using System;

namespace H2_string_interpolation
{
    class Program
    {
        static void Main(string[] args)
        {
            byte factor1 = 1;
            int factor2 = 411;
            Console.WriteLine($"{factor1} * {factor2} = {factor1++ * factor2}");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"{factor1} * {factor2} = {factor1++ * factor2}");
            Console.ReadKey();
            Console.Clear();
            // en nu terug naar af
            Console.WriteLine($"{factor1} * {factor2} = {factor1-- * factor2}");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine($"{factor1} * {factor2} = {factor1-- * factor2}");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
