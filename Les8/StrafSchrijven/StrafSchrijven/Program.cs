﻿using System;

namespace StrafSchrijven
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Straf schrijven");
            StrafSchrijvenOpdracht();
            Console.WriteLine("Druk op een toets om de console te sluiten.");
            Console.ReadKey();
        }

        static void StrafSchrijvenOpdracht()
        {
            Console.WriteLine("Straf schrijven");
            bool blijfSchrijven = true;
            while (blijfSchrijven)
            {
                for (int i = 1; i <= 99; i++)
                {
                    if (i == 1 || i == 8 || i >= 20)
                    {
                        Console.WriteLine($"{i}ste regel: ik zal zwijgen in de klas");
                    }
                    else
                    {
                        Console.WriteLine($"{i}de regel: ik zal zwijgen in de klas");
                    }
                }
                Console.WriteLine("\nNog een keer de straf schrijven J/N?");
                char antwoord = Console.ReadKey().KeyChar;
                if (antwoord == 'N')
                {
                    blijfSchrijven = false;
                }
            }
        }
    }
}
