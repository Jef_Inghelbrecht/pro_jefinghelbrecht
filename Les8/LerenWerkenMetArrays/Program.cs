﻿using System;
using Inghelbrecht;

namespace LerenWerkenMetArrays
{
    // enum PrintType {Int, Ascii, Hex, SpongeBob, Binary };
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met arrays");
            //// array vullen met 0 tot 10
            //int[] getallen = new int[11];
            //for (int i = 0; i <= getallen.Length-1; i++)
            //{
            //    getallen[i] = i;
            //}

            //Console.WriteLine("Inhoud van de array:");
            //for (int i = 0; i <= getallen.Length-1; i++)
            //{
            //    Console.WriteLine($"element {i} bevat {i}");
            //}
            //Console.WriteLine("Inhoud van de array in omgekeerde volgorde:");
            //for (int i = getallen.Length-1; i >= 0; i--)
            //{
            //    Console.WriteLine($"element {i} bevat {i}");
            //}
            int start = 100;
            int einde = 150;
            int[] nieuweArray = Inghelbrecht.Jef.VulIntArrayAsc(start, einde);


            PrintType printType = PrintType.Ascii;



            if (printType == PrintType.Int)
            {
                for (int i = start; i < einde; i++)
                {
                    Console.WriteLine($"{nieuweArray[i - start]}\n");
                }
            }
            else if (printType == PrintType.Ascii)
            {
                for (int i = start; i < einde; i++)
                {
                    Console.WriteLine($"{Convert.ToChar(nieuweArray[i - start])}\n");
                }
            }

            Console.WriteLine(Jef.PrintIntArray(nieuweArray, 1));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc(100, 1), PrintType.Int));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayAsc('a', 'z'), 2));
            Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc('A', '\b'), PrintType.Binary));
            // sentitel
            bool done = false;
            do
            {
                Console.WriteLine("1. getallen tussen 200 en 400");
                Console.WriteLine("2. getallen van  100 naar 1");
                Console.WriteLine("3. letters van a tot z");
                Console.WriteLine("4. letters van A tot \\b");
                Console.WriteLine("5. getallen van  100 naar 1 in hex");
                Console.WriteLine("0. Stop");

                char keuze = Console.ReadKey().KeyChar;
                switch (keuze)
                {
                    case '1':
                        int[] nogEenArray = Inghelbrecht.Jef.VulIntArrayAsc(200, 400);
                        Console.WriteLine(Jef.PrintIntArray(nogEenArray, 1));
                        break;
                    case '2':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc(100, 1), PrintType.Int));
                        break;
                    case '3':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayAsc('a', 'z'), PrintType.Ascii));
                        break;

                    case '4':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc('A', '\b'), PrintType.Binary));
                        break;
                    case '5':
                        Console.WriteLine(Jef.PrintIntArray(Jef.VulIntArrayDesc(100, 1), PrintType.Hex));
                        break;
                    case '0':
                        done = true;
                        break;
                }
            } while (!done);

            Console.ReadLine();
        }


    }
}
