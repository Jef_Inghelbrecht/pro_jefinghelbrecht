﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inghelbrecht
{
    enum PrintType { Int, Ascii, Hex, Binary, SpongeBob };
    class Jef
    {
        public static int[] VulIntArrayAsc(int start, int einde)
        {
            int[] getallen = new int[(einde + 1) - start];
            for (int i = start; i <= einde; i++)
            {
                getallen[i - start] = i;
            }
            return getallen;
        }

        public static int[] VulIntArrayDesc(int einde, int start)
        {
            int[] getallen = new int[(einde + 1) - start];
            for (int i = einde; i >= start; i--)
            {
                getallen[einde - i] = i;
            }
            return getallen;
        }

        // overloading: zelfde methodenaam, andere parameters
        public static int[] VulIntArrayAsc(char start, char einde)
        {
            return VulIntArrayAsc(Convert.ToInt32(start), Convert.ToInt32(einde));
        }

        public static int[] VulIntArrayDesc(char einde, char start)
        {
            return VulIntArrayDesc(Convert.ToInt32(einde), Convert.ToInt32(start));
        }
        // Als type = 1, print getallen, als type = 2 print karakters
        public static string PrintIntArray(int[] getallen, int type)
        {
            string tekst = "De inhoud van de array is:\n";
            for (int i = 0; i <= getallen.Length - 1; i++)
            {
                switch (type)
                {
                    case 1:
                        tekst += $"{getallen[i]}\n";
                        break;
                    case 2:
                        tekst += $"{Convert.ToChar(getallen[i])}\n";
                        break;
                    case 3:
                        tekst += $"{Convert.ToString(getallen[i], 16).ToString()}\n";
                        break;
                    case 4:
                        tekst += $"{Convert.ToString(getallen[i], 2).ToString()}\n";
                        break;
                }
            }
            return tekst;
        }

        public static string PrintIntArray(int[] getallen, PrintType type)
        {
            string tekst = "De inhoud van de array is:\n";
            for (int i = 0; i <= getallen.Length - 1; i++)
            {
                switch (type)
                {
                    case PrintType.Int:
                        tekst += $"{getallen[i]}\n";
                        break;
                    case PrintType.Ascii:
                        tekst += $"{Convert.ToChar(getallen[i])}\n";
                        break;
                    case PrintType.Hex:
                        tekst += $"{Convert.ToString(getallen[i], 16).ToString()}\n";
                        break;
                    case PrintType.Binary:
                        tekst += $"{Convert.ToString(getallen[i], 2).ToString()}\n";
                        break;
                    case PrintType.SpongeBob:
                        tekst += $"{Convert.ToString(getallen[i], 8).ToString()}\n";
                        break;

                }
            }
            return tekst;
        }
    }
}
