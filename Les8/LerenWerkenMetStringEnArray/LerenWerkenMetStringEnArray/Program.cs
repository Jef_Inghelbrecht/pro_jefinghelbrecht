﻿using System;

namespace LerenWerkenMetStringEnArray
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met strings en array's!");
            string mijnNaam = LeesWoordIn("Type je naam in: ");
            string naamVanHond = LeesWoordIn("Typ de naam van je hond, heb je er geen, typ dan 'geen'");
            string mijnPartner = LeesWoordIn("Type de naam van je partner in: ");

            string[] info = { mijnNaam, naamVanHond, "Clio", "Antwerpen", mijnPartner };
            PrintStringArray(info);

            char[] charArray = StringToArray(mijnPartner);
            Console.Write($"De test: {charArray} bestaat uit de volgende letters: ");
            PrintCharArray(charArray);
            Console.ReadLine();
        }

        static string LeesWoordIn(string titel)
        {
            Console.Write(titel);
            return Console.ReadLine();
        }

        static void PrintStringArray(string[] stringArray)
        {
            for (int i = 0; i < stringArray.Length; i++)
            {
                Console.WriteLine(stringArray[i]);

            }
        }

        static char[] StringToArray(string tekst)
        {
            char[] charArray = new char[tekst.Length];
            // lange weg
            for (int i = 0; i < tekst.Length; i ++)
            {
                charArray[i] = Convert.ToChar(tekst.Substring(i, 1));
            }

            // korte weg
            charArray = tekst.ToCharArray();
            // moet een array van char's retourneren
            return charArray;
        }

        static void PrintCharArray(char[] charArray)
        {
            for (int i = 0; i < charArray.Length; i++)
            {
                Console.WriteLine($"de letter {charArray[i]}\n");
            }
        }
    }
}
